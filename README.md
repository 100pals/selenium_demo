Selenium Demo
=============

A small demo showing how to capture screenshots from SteamHunters.com for integrating into other projects.

Requirements
------------

Docker, docker-compose

Instructions
------------

If you have docker-compose set up locally, you should be able to just clone the repo, replace `/home/jippen/src/100pals/selenium_demo` with the current path to this directory, then run `docker-compose up` - and it should take a screenshot of the profile you aim it at, and save it in the output folder

Known issues
-------------

Being a demo, this has a few known problems.

When you run docker-compose up, it spins up two containers, one runs and finishes immediately, and the other stays running. So you'll have to `Ctrl + C` to shut it down after seeing the line `selenium_demo_client_1 exited with code 0`

$PWD and relative paths don't work in docker-compose.yaml files for some reason, so I had to hardcode things. This is dumb, but its also a demo.


#!/usr/bin/env python3
import time
from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.action_chains import ActionChains
from selenium import webdriver
from selenium.webdriver.common.by import By
from PIL import Image
from io import BytesIO


def get_sh_screenshot(steamid):
    url = f"https://steamhunters.com/id/{steamid}/achievements?view=smallgrid&sort=sh"

    # Set up virtual browser
    profile = webdriver.FirefoxProfile()
    options = webdriver.FirefoxOptions()
    driver = webdriver.Remote(
        command_executor="http://selenium_demo_server:4444/wd/hub",
        desired_capabilities=options.to_capabilities(),
    )

    driver.get(url)
    time.sleep(5)  # just in case

    element = driver.find_elements(by=By.CSS_SELECTOR, value="div.container")[0]
    actions = ActionChains(driver)
    actions.move_to_element(element).perform()
    time.sleep(5)

    size = driver.get_window_size()
    driver.set_window_size(size["width"], 1000 + size["height"])
    tmpfile = "/tmp/ss.png"
    driver.save_screenshot(tmpfile)

    # Grab screenshot
    with open(tmpfile, "rb") as fh:
        im = Image.open(fh)

        # Crop down
        left = 66
        top = 390
        right = 1281
        bottom = 1385
        im = im.crop((left, top, right, bottom))

        # Save the result
        im.save(f"../output/{steamid}.png")

    driver.close()


if __name__ == "__main__":
    time.sleep(10)  # Wait for container to boot
    get_sh_screenshot("jippenfaddoul")

FROM python:3
WORKDIR /usr/src/app

ENV DEBIAN_FRONTEND noninteractive

COPY requirements.txt .

RUN set -x \
   && apt update \
   && apt upgrade -y \
   && python -m pip install -r requirements.txt \
   && apt-get clean -y
 
# Do actual stuff for my app
COPY app/ .
CMD ["python", "main.py"]